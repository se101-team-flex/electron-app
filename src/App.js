import React from 'react';
import Home from './Home';
import Splash from './Splash';
import { HashRouter as Router, Route } from 'react-router-dom';

class App extends React.Component {

  render() {
    return (
      <Router>
        <Route exact path="/" component={Splash} />
        <Route path="/home" component={Home} />
      </Router>
    );
  }



}
export default App;
