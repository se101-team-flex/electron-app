import React from 'react';
import { Grommet, Box, Text } from 'grommet';

class Greeting extends React.Component {

    months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];

    state = {
        time: null,
        month: null,
        year: null
        //name: this.props.name
    };

    componentDidMount() {
        let date = null;

        date = new Date();
        this.setState({
            time: date.getHours(),
            day: this.days[date.getDay()],
            date: date.getDate(),
            month: this.months[date.getMonth()],
            year: date.getFullYear()
        }
        )
    }

    render() {

        let str = this.state.time < 12 ? `Good Morning, ${this.props.name}.` : `Good Afternoon, ${this.props.name}.`;
        let { day, date, month, year } = this.state;

        console.log(`${day}, ${date}, ${month}, ${year}`)

        const theme = {
            global: {
                font: {
                    family: 'Poppins',
                    size: '18px',
                    height: '20px',
                },
                margin: "xlarge"
            }
        }
        return (
            <Grommet theme={theme}>
                <Box direction="column">
                    <Text size="xxlarge">{str}</Text>
                    <Text size="large">It is {day}, {month} {date}, {year}</Text>
                </Box>
            </Grommet>
        );
    }
}

export default Greeting;