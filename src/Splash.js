import React from 'react';
import { Grommet, Box, Text } from 'grommet';
import logo from './assets/logo.png';
import { Redirect } from 'react-router-dom';
import { PythonShell } from 'python-shell';

class Splash extends React.Component {

    state = {
        auth: true,
        user: {
            name: ""
        }
    }

    componentDidMount() {

        let pyshell = new PythonShell('./lib/auth/interface.py');

        pyshell.send('--authenticate --base-dir lib/auth');

        pyshell.on('message', (message) => {
            console.log(message);
            this.setState({
                auth: true,
                user: {
                    name: message
                }
            })
        });
        console.log(this.state);
    }

    render() {
        const theme = {
            global: {
                font: {
                    family: 'Poppins',
                    size: '18px',
                    height: '20px',
                }
            }
        };

        return (
            < Grommet theme={theme} >
                {this.state.auth ? <Redirect to={{ pathname: '/home', state: { name: "Jack" } }} /> : null}
                <Box direction='column' background={{ color: 'black' }} align='center' justify='center' height="100vh">
                    <img src={logo} width={512} height={512} className='Logo' />
                </Box>
            </Grommet >
        );
    }
}

export default Splash;