const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

const path = require('path');
const isDev = require('electron-is-dev');

let mainWindow;

app.commandLine.appendSwitch('widevin-cdm-path', '../src/assets/widevinecdm.dll');
app.commandLine.appendSwitch("widevine-cdm-version", "4.10.1582.2");

function createWindow() {
    mainWindow = new BrowserWindow({
        width: 720, height: 1280, frame: false, webPreferences: {
            nodeIntegration: true,
            devTools: false
        }
    });
    mainWindow.loadURL(isDev ? 'http://localhost:3000' : `file://${path.join(__dirname, '../build/index.html')}`);
    if (isDev) {
        // Open the DevTools.
        //BrowserWindow.addDevToolsExtension('<location to your react chrome extension>');
        mainWindow.webContents.openDevTools();
    }
    mainWindow.on('closed', () => mainWindow = null);
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', () => {
    if (mainWindow === null) {
        createWindow();
    }
});