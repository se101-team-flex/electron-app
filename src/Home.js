import React from 'react';
import { Grommet, Box, Clock } from 'grommet';
import { Microphone } from 'grommet-icons';
import Greeting from './components/Greet';
import Weather from './components/Weather';
import SpotifyPlayer from 'react-spotify-player';


class Home extends React.Component {
    state = {
        date: new Date()
    }


    render() {
        const theme = {
            global: {
                font: {
                    family: 'Poppins',
                    size: '18px',
                    height: '20px',
                },
                margin: "xlarge"
            }
        }

        return (
            <Grommet theme={theme} >
                <Box background={{ color: 'black' }} height="100vh" direction="column">
                    <Greeting name={"Jack"} />
                    <Clock type="digital" size="xxlarge" />
                    <Weather />
                    <Box margin="25px"></Box>
                    <SpotifyPlayer
                        uri="spotify:album:7uVimUILdzSZG4KKKWToq0"
                        size="large"
                    />
                </Box>
            </Grommet>
        );
    }

}

export default Home;