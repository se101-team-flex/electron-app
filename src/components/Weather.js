import React, { Component } from 'react';
import $ from 'jquery';
import { Grommet } from 'grommet';

class Weather extends Component {

    state = {
        data: {
            main: {
                temp: 3,
                temp_max: 5,
                temp_min: -4,
            },
            weather: [{
                description: "light clouds",
                main: ""
            }],
            name: "Waterloo"
        },
    }

    async componentDidMount() {
        let weatherData = await $.getJSON("https://api.openweathermap.org/data/2.5/weather?q=Waterloo,CA&APPID=8b45f33c442800efa23cac73636bb53f");

        console.log(weatherData);

        this.setState({
            data: weatherData
        });
    }

    render() {

        let { data } = this.state;

        const theme = {
            global: {
                font: {
                    family: 'Poppins',
                    size: '18px',
                    height: '20px',
                }
            }
        }

        return (
            <Grommet theme={theme}>
                <h2 className="City">
                    {data.name}
                </h2>

                <h1 className="Temp">
                    {Math.round(data.main.temp - 273.15)}°C
                </h1>

                <p className="Details">
                    Today, there's a high of {Math.round(data.main.temp_max - 273.15)}°C and a low of {Math.round(data.main.temp_min - 273.15)}°C. Expect {data.weather[0].description}.
                </p>

            </Grommet>
        );
    }
}

export default Weather;