import { deepFreeze } from "grommet/utils"

export default const FlexTheme =
    {
        global: {
            colors: {
                active: "rgba(102,102,102,0.5)",
                border: "#000000",
                brand: "#FD6FFF",
                control: "#60EBE1",
                focus: "#FFCA58",
                text: "#eeeeee",
                accent- 1: "#FD6FFF",
            accent- 2: "#60EB9F",
        accent- 3: "#60EBE1",
            neutral - 1: "#EB6060",
                neutral - 2: "#01C781",
                    neutral - 3: "#6095EB",
                        neutral - 4: "#FFB200",
                            status - critical: "#FF3333",
                                status - error: "#FF3333",
                                    status - warning: "#F7E464",
                                        status - ok: "#7DD892",
                                            status - unknown: "#a8a8a8",
                                                status - disabled: "#a8a8a8"
            },
drop: {
    background: "#333333"
},
focus: {
    border: {
        color: [
            null,
            ";"
        ]
    }
},
input: {
    weight: 700
}
        },
anchor: {
    color: "#FFCA58"
},
button: {
    border: {
        radius: "0px"
    }
},
layer: {
    background: "#111111",
        overlay: {
        background: "rgba(48,48,48,0.5)"
    }
}
    }